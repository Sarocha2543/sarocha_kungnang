package com.example.sarocha_kungnang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void Profile (View view) {
        Intent intent = new Intent( this,MainActivity.class);
        startActivity(intent);
    }
    public void Facebook(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("https://web.facebook.com/profile.php?id=100005065304641"));
        startActivity(browserIntent);
    }
    public void Instagram(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.instagram.com/kung_sarocha/?hl=en"));
        startActivity(browserIntent);
    }
}